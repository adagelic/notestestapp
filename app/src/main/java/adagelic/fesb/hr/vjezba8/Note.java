package adagelic.fesb.hr.vjezba8;

/**
 * Created by adagelic on 09/01/17.
 */
public class Note {
    private	int	id;
    private	String	title;
    private	String	content;

    public int getId() {
        return id;
    }

    public void	setId(int id) {
        this.id	= id;
    }

    public String getTitle() {
        return title;
    }

    public void	setTitle(String	title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void	setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return this.title;
    }

}
