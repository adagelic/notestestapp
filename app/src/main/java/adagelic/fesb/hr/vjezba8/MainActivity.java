package adagelic.fesb.hr.vjezba8;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button newNoteBtn = (Button) findViewById(R.id.newNoteBtn);
        Button notesListBtn = (Button) findViewById(R.id.notesListBtn);

        newNoteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newNoteActivityIntent = new Intent(MainActivity.this, NewNoteActivity.class);
                startActivity(newNoteActivityIntent);
            }
        });

        notesListBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent notesListActivityIntent = new Intent(MainActivity.this, NotesListActivity.class);
                startActivity(notesListActivityIntent);
            }
        });
    }
}
