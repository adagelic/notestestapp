package adagelic.fesb.hr.vjezba8;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by adagelic on 09/01/17.
 */
public class NotesDataSource {

    private SQLiteDatabase database;
    private	MySQLiteHelper	dbHelper;

    public NotesDataSource(Context context)	{
        dbHelper = new MySQLiteHelper(context);

        try {
            this.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void	open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void	close()	{
        database.close();
    }

    public void addNoteToDb(String title, String content) {
        ContentValues values = new ContentValues();
        values.put("title",	title);
        values.put("content", content);
        database.insert("notes", null, values);
    }

    public Note getNoteById(int id) {
        Note noteToReturn = new Note();

        Cursor cursor = database.rawQuery("SELECT * FROM notes WHERE id = '" + String.valueOf(id) + "'", null);

        cursor.moveToFirst();

        if (! cursor.isAfterLast()) {
            noteToReturn.setId(cursor.getInt(0));
            noteToReturn.setTitle(cursor.getString(1));
            noteToReturn.setContent(cursor.getString(2));
        }

        return noteToReturn;
    }

    public ArrayList<Note> getAllNotes()	{
        ArrayList<Note>	notes = new	ArrayList<Note>();

        Cursor cursor =	database.rawQuery("SELECT *	FROM notes", null);
        cursor.moveToFirst();

        while(!	cursor.isAfterLast()) {
            Note note =	new	Note();
            note.setId(cursor.getInt(0));
            note.setTitle(cursor.getString(1));
            note.setContent(cursor.getString(2));
            notes.add(note);
            cursor.moveToNext();
        }

        cursor.close();

        Log.d("ANTE", "Ready " + String.valueOf(notes.size()) + " notes");
        return	notes;
    }

}
