package adagelic.fesb.hr.vjezba8;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by adagelic on 09/01/17.
 */
public class NewNoteActivity extends AppCompatActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_note_layout);

        final EditText titleEt = (EditText) findViewById(R.id.titleEt);
        final EditText contentEt = (EditText) findViewById(R.id.contentEt);
        Button saveNoteBtn = (Button) findViewById(R.id.saveNoteBtn);

        saveNoteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // save note to DB
                NotesDataSource notesDataSource = new NotesDataSource(getApplicationContext());
                notesDataSource.addNoteToDb(titleEt.getText().toString(), contentEt.getText().toString());

                // toast that its saved
                Toast.makeText(getApplicationContext(), "New note added: " + titleEt.getText().toString(), Toast.LENGTH_LONG).show();

                // return to main menu
                finish();
            }
        });

    }

}
