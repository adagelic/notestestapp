package adagelic.fesb.hr.vjezba8;

import java.util.ArrayList;

/**
 * Created by adagelic on 09/01/17.
 */
public class DataStorage {

    public static ArrayList<Note> allNotesList = new ArrayList<Note>();


    public static Note getNoteById(Integer id) {
        for (int i = 0; i < allNotesList.size(); i++) {
            if (allNotesList.get(i).getId() == id) {
                return allNotesList.get(i);
            }
        }

        return null;
    }

}
