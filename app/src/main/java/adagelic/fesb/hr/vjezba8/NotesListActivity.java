package adagelic.fesb.hr.vjezba8;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

/**
 * Created by adagelic on 09/01/17.
 */
public class NotesListActivity extends AppCompatActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notes_list_layout);

        // fill data storage from database
        NotesDataSource notesDataSource = new NotesDataSource(getApplicationContext());

        DataStorage.allNotesList = notesDataSource.getAllNotes();

        ListView notesListView = (ListView) findViewById(R.id.notesListView);

        notesListView.setAdapter(new NotesListAdapter(getApplicationContext()));

        notesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Note clickedNote = DataStorage.allNotesList.get(position);

                Intent openNoteIntent = new Intent(NotesListActivity.this, OpenNoteActivity.class);
                openNoteIntent.putExtra("opened-note", clickedNote.getId());

                startActivity(openNoteIntent);
            }
        });
    }

}
