package adagelic.fesb.hr.vjezba8;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * Created by adagelic on 09/01/17.
 */
public class OpenNoteActivity extends AppCompatActivity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.open_note_layout);

        Note openedNote = null;

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            Integer noteId = extras.getInt("opened-note");
            openedNote = DataStorage.getNoteById(noteId);
        }

        if (openedNote != null) {
            TextView titleTv = (TextView) findViewById(R.id.noteTitleTv);
            TextView contentTv = (TextView) findViewById(R.id.noteContentTv);
            TextView noteIdTitle = (TextView) findViewById(R.id.openNoteTitle);

            titleTv.setText(openedNote.getTitle());
            contentTv.setText(openedNote.getContent());
            noteIdTitle.setText("Note ID " + String.valueOf(openedNote.getId()) + ":");
        }


    }

}
